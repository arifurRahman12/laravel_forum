<?php

namespace App;

class Channel extends Model
{
    public function discussion()
    {
        return $this->hasMany(Discussion::class);
    }
}
