<?php

namespace App;

use App\Notifications\MarkedAsBestReply;

class Discussion extends Model
{
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function bestReply(Reply $reply)
    {
        $this->update([
            'reply_id' => $reply->id
        ]);

        if($this->author->id === $reply->owner->id){
            return;
        }

        $reply->owner->notify(new MarkedAsBestReply($reply->discussion));
    }

    public function getBestReply()
    {
        return $this->belongsTo(Reply::class, 'reply_id');
    }

    public function scopeFilterByChannels($builder)
    {
        if(request()->query('channel')){
            // filter
            $channel = Channel::where('slug', request()->query('channel'))->first();

            if($channel){
                return $builder->where('channel_id', $channel->id);
            }

            return $builder;
        }

        return $builder;
    }
}
