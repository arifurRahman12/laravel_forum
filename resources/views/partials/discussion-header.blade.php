<div class="card-header text-white bg-dark">
    <div class="d-flex justify-content-between">
        <div>
            <img height="40px" weight="40px" class="rounded-circle rounded-sm" src="{{ Gravatar::src($discussion->author->email) }}" alt="">
            <span class="ml-2 lead">{{ $discussion->author->name }}</span>
        </div>
        <div>
            <a href="{{ route('discussion.show', $discussion->slug ) }}" class="btn btn-success">View</a>
        </div>
    </div>

</div>
