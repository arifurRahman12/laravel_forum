@extends('layouts.app')

@section('content')
    <div class="card mb-5">

        @include('partials.discussion-header')

        <div class="card-body">
            <div class="text-center font-weight-bold text-info">
                {{ $discussion->title }}
            </div>

            <hr>

            {!! $discussion->content !!}

            @if($discussion->getBestReply)
                <div class="card bg-success mt-5 text-white">
                    <div class="card-header d-flex justify-content-between">
                        <div class="lead">
                            <img width="40px" height="40px" class="rounded-circle mr-2" src="{{ Gravatar::src($discussion->getBestReply->owner->email) }}">
                            {{ $discussion->getBestReply->owner->name }}
                        </div>
                        <strong>Best Reply</strong>
                    </div>
                    <div class="card-body py-1">
                        {!! $discussion->getBestReply->reply !!}
                    </div>
                </div>
            @endif
        </div>
    </div>

    @foreach($discussion->replies()->paginate(2) as $reply)
    <div class="card border-primary mb-1">
        <div class="d-flex justify-content-between card-header py-1">
            <div>
                <img height="40px" weight="40px" class="rounded-circle rounded-sm" src="{{ Gravatar::src($reply->owner->email) }}">
                <span class="ml-2">{{ $reply->owner->name }}</span>
            </div>
            <div>
                @auth()
                    @if(auth()->user()->id === $discussion->user_id)
                        <form action="{{ route('best-reply', ['discussion' => $discussion->slug, 'reply' => $reply->id ]) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn btn-primary btn-sm mt-1">mark as best reply</button>
                        </form>
                    @endif
                @endauth

            </div>
        </div>
            <div class="card-body py-1">
                {!! $reply->reply !!}
            </div>
    </div>
    @endforeach
    {{ $discussion->replies()->paginate(2)->links() }}

    <div class="card border-dark">
        <div class="card-header">
            Add a reply
        </div>
        <div class="card-body">
            @auth()
                <form action="{{ route('replies.store', $discussion->slug ) }}" method="POST">
                    @csrf
                    <input id="reply" type="hidden" name="reply">
                    <trix-editor input="reply"></trix-editor>
                    <button class="btn btn-success btn-sm mt-2">Add Reply</button>
                </form>
            @else
                <a href="{{ route('login') }}" class="btn btn-info text-white">Sign in to add a reply</a>
            @endauth
        </div>
    </div>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('css/trix.css') }}">
@endsection

@section('script')
    <script src="{{ asset('js/trix.js') }}"></script>
@endsection

