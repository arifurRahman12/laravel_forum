@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header bg-danger text-white">Notifications</div>

        <div class="card-body">
            <ul class="list-group">
                @foreach($notifications as $notification)
                    <li class="list-group-item">
                        @if($notification->type === 'App\Notifications\NewReplyAdded')
                            A new reply added to your discussion -
                            <a href="{{ route('discussion.show', $notification->data['discussion']['slug']) }}" class="text-danger lead ml-1">
                                {{ $notification->data['discussion']['title'] }}
                            </a>
                        @endif
                        @if($notification->type === 'App\Notifications\MarkedAsBestReply')
                            your reply on
                                <a href="{{ route('discussion.show', $notification->data['discussion']['slug']) }}" class="text-danger lead mx-1">
                                    {{ $notification->data['discussion']['title'] }}
                                </a>
                            has been marked as best reply
                            @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
