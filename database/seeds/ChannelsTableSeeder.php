<?php

use Illuminate\Database\Seeder;
use App\Channel;
use Illuminate\Support\Str;

class ChannelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Channel::create([
            'name' => 'Laravel 6',
            'slug' => Str::slug('Laravel 6')
        ]);

        Channel::create([
            'name' => 'Vue JS',
            'slug' => Str::slug('Vue JS')
        ]);

        Channel::create([
            'name' => 'React JS',
            'slug' => Str::slug('React JS')
        ]);

        Channel::create([
            'name' => 'Angular JS',
            'slug' => Str::slug('Angular JS')
        ]);
    }
}
